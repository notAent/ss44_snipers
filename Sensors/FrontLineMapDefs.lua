local sensorInfo = {
	name = "FrontLineMapDefs",
	desc = "Provide constants for mapping sensoring system",
	author = "PepeAmpere",
	date = "2019-06-27",
	license = "MIT",
}

local EVAL_PERIOD_DEFAULT = math.huge

local TILE_SIZE = 256 -- in elmos

local mapX =  Game.mapSizeX
local mapZ = Game.mapSizeZ
local mapTilesX = math.ceil(mapX / TILE_SIZE)
local mapTilesZ = math.ceil(mapZ / TILE_SIZE)
local mapArraySize = mapTilesX * mapTilesZ
local mapCoordinates = {}
local x = -TILE_SIZE
local z = 0
for i=1, mapArraySize do	
	if (i % mapTilesZ == 1) then
		z = 0
		x = x + TILE_SIZE
	else
		z = z + TILE_SIZE
	end
	mapCoordinates[i] = {
		topLeft = Vec3(x, Spring.GetGroundHeight(x,z), z),
		bottomRight = Vec3(x + TILE_SIZE, Spring.GetGroundHeight(x + TILE_SIZE, z + TILE_SIZE), z + TILE_SIZE),
		middle = Vec3(x + TILE_SIZE/2, Spring.GetGroundHeight(x + TILE_SIZE/2, z + TILE_SIZE/2), z + TILE_SIZE/2),
	}
end

local function ValidateMapPosition(position)
	if position.x < 0 then position.x = 0 end
	if position.z < 0 then position.z = 0 end
	if position.x > mapX then position.x = mapX end
	if position.z > mapZ then position.z = mapZ end
	return position
end

local function GetTileIndexForPosition(position)
	local ix = math.floor(position.x / TILE_SIZE)
	local iz = math.floor(position.z / TILE_SIZE)
	return (ix * mapTilesZ + iz) + 1
end

local function GetCornerIndexes(position, tileRadius)
	local bottomLeftRelative = Vec3(-tileRadius*TILE_SIZE, 0 , tileRadius*TILE_SIZE)
	local bottomRightRelative = Vec3(tileRadius*TILE_SIZE, 0 , tileRadius*TILE_SIZE)
	local topLeftPosition = ValidateMapPosition(position - bottomRightRelative)
	local topRightPosition = ValidateMapPosition(position - bottomLeftRelative)
	local bottomLeftPosition = ValidateMapPosition(position + bottomLeftRelative)
	local bottomRightPosition = ValidateMapPosition(position + bottomRightRelative)
	
	--Spring.Echo(topLeftPosition, topRightPosition, topRightPosition, bottomRightPosition)
	--Spring.Echo(position - bottomRightRelative, position - bottomLeftRelative, position + bottomLeftRelative, position + bottomRightRelative)
	return GetTileIndexForPosition(topLeftPosition), 
	GetTileIndexForPosition(topRightPosition),
	GetTileIndexForPosition(bottomLeftPosition),
	GetTileIndexForPosition(bottomRightPosition)	
end

local function GetUpdatePool(currentMap, position, tileRadius)
	local topLeftIndex, topRightIndex, bottomLeftIndex, bottomRightIndex = GetCornerIndexes(position, tileRadius)
	local updatePoolCoordinates = {}
	local updatePoolOldData = {}
	local updatePoolIndexes = {}
	local poolIndex = 1
	local zSteps = 0
	
	for i=topLeftIndex, bottomRightIndex do
		updatePoolCoordinates[poolIndex] = mapCoordinates[i]
		updatePoolOldData[poolIndex] = currentMap[i]
		updatePoolIndexes[poolIndex] = i
		poolIndex = poolIndex + 1
		if i % mapTilesZ > i % topRightIndex then
			zSteps = zSteps + 1
			i = topLeftIndex + zSteps * mapTilesZ
		end
	end
	
	return updatePoolCoordinates, updatePoolOldData, updatePoolIndexes
end

local function NewTile()
	return {
		allySoldiersCount = 0,
		allyVehiclesCount = 0,
		enemySoldierCount = 0,
		enemyVehiclesCount = 0,
		enemyLastReport = 0,
		updateFrame = 0,
		inLOS = false,
	}
end

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- @description Recalculate 
return function()
	return {
		TILE_SIZE = TILE_SIZE,
		mapX = mapX,
		mapZ = mapZ,
		mapTilesX = mapTilesX,
		mapTilesZ = mapTilesZ,
		mapArraySize = mapArraySize,
		mapCoordinates = mapCoordinates,
		
		GetCornerIndexes = GetCornerIndexes,
		GetTileIndexForPosition = GetTileIndexForPosition,
		GetUpdatePool = GetUpdatePool,
		NewTile = NewTile,
		ValidateMapPosition = ValidateMapPosition,
	}
end
