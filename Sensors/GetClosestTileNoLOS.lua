local sensorInfo = {
	name = "GetClosestTileNoLOS",
	desc = "Give index of tile which is not on LOS",
	author = "PepeAmpere",
	date = "2019-06-28",
	license = "MIT",
}

local EVAL_PERIOD_DEFAULT = -1
local mapDefs = Sensors.FrontLineMapDefs()

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- @description Give index of tile which is not on LOS
-- @argument currentMap [array] full threat map
return function(currentMap)
	if (currentMap == nil) then
		return FAIL
	end
	
	local updatePoolCoordinates, updatePoolOldData, updatePoolIndexes = GetUpdatePool(currentMap, currentPosition, tileRadius)
	
	local 
	for i=1, #updatePoolOldData do
		if ()
	end
end
