local sensorInfo = {
	name = "FrontLineMapClosestNotInLOS",
	desc = "Return closes tile which is not in LOS",
	author = "PepeAmpere",
	date = "2019-06-22",
	license = "notAlicense",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load
attach.Module(modules, "tableExt") --  table module

local EVAL_PERIOD_DEFAULT = -1 -- this sensor is not caching any values, it evaluates itself on every request
local TILE_SIZE = 256 -- in elmos
local mapX = Game.mapSizeX 
local mapZ = Game.mapSizeZ
local mapTilesX = math.ceil(mapX / TILE_SIZE)
local mapTilesZ = math.ceil(mapZ / TILE_SIZE)
local mapArraySize = mapTilesX * mapTilesZ
local mapCoordinates = {}

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

local function GetTileIndexForPosition(position)
	local ix = math.floor(position.x / TILE_SIZE)
	local iz = math.floor(position.z / TILE_SIZE)
	return (ix * mapTilesZ + iz) + 1
end

local function GetCornerIndexes(position, tileRadius)
	local cornerRelativePosition = Vec3(tileRadius*TILE_SIZE, 0 , tileRadius*TILE_SIZE)
	local topLeftPosition = position - cornerRelativePosition
	local bottomRightPosition = position + cornerRelativePosition
	return GetTileIndexForPosition(topLeftPosition), GetTileIndexForPosition(bottomRightPosition)
end

-- @description Recalculate values for specified areas
-- @argument currentMap [array] previous full threat map
-- @argument currentPosition [Vec3|optional] center of update radius (in not provided, we take position of the point-unit)
-- @argument tileRadius [number] radius of map update in tiles
-- @return currentMap [array] updated version of map
-- @comment map is supposed to be stored
return function(currentMap, currentPosition, tileRadius)

	
	return currentMap
end
