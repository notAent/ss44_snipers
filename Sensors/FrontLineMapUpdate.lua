local sensorInfo = {
	name = "FrontLineMapUpdate",
	desc = "Update function. Ready for sequential evaluation of many areas at the same time.",
	author = "PepeAmpere",
	date = "2019-01-17",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- this sensor is never caching its values
local MY_ALLY_ID = Spring.GetMyAllyTeamID()
local OVERVIEW_HEIGHT = 10
local UPDATE_TRESHOLD = 60

local SpringGetUnitsInRectangle = Spring.GetUnitsInRectangle
local SpringGetGroundHeight = Spring.GetGroundHeight
local SpringGetPositionLosState = Spring.GetPositionLosState

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- @description Recalculate values for specified areas
-- @argument areasSpecs [array] list of areas which should be recalculated 
-- @return newValues [array] array of tables of final HP values
return function(areasSpecs, areasOldData)
	local newValues = {}
	local updateFrame = Spring.GetGameFrame()
	for a=1, #areasSpecs do
		local area = areasSpecs[a]
		local oldData = areasOldData[a]
		local allySoldiersCount = 0
		local allyVehiclesCount = 0
		local enemySoldierCount = 0
		local enemyVehiclesCount = 0
		
		local losOrRadar, inLOS, inRadar, jammed = SpringGetPositionLosState(area.middle.x, area.middle.y + OVERVIEW_HEIGHT, area.middle.z, MY_ALLY_ID)
		
		local alreadyCalculatedThisFrame = false
		if oldData ~= nil then
			if updateFrame - oldData.updateFrame < UPDATE_TRESHOLD then alreadyCalculatedThisFrame = true end
		end
		
		if (inLOS or oldData == nil) and not alreadyCalculatedThisFrame then
			
			local allUnits = SpringGetUnitsInRectangle(area.topLeft.x, area.topLeft.z, area.bottomRight.x, area.bottomRight.z)
			for u=1, #allUnits do
				local unitID = allUnits[u]
				local unitDefID = Spring.GetUnitDefID(unitID)
				local isAllied = Spring.IsUnitAllied(unitID)
				local health, fullHealth = Spring.GetUnitHealth(unitID)
				
				local isVehicle = false
				if UnitDefs[unitDefID] ~= nil then
					if UnitDefs[unitDefID].moveDef.xsize ~= nil then
						if UnitDefs[unitDefID].moveDef.xsize > 1 then isVehicle = true end
					end
				end
				
				if isAllied then
					if isVehicle then
						allyVehiclesCount = allyVehiclesCount + 1
					else
						allySoldiersCount = allySoldiersCount + 1
					end
				else
					if isVehicle then
						enemyVehiclesCount = enemyVehiclesCount + 1
					else
						enemySoldierCount = enemySoldierCount + 1
					end
				end
			end
			
			local enemyLastReport = 0
			if enemySoldierCount > 0 or enemyVehiclesCount > 0 then
				enemyLastReport = updateFrame
			elseif oldData ~= nil then
				enemyLastReport = oldData.enemyLastReport
			end
			
			newValues[a] = {
				allySoldiersCount = allySoldiersCount,
				allyVehiclesCount = allyVehiclesCount,
				enemySoldierCount = enemySoldierCount,
				enemyVehiclesCount = enemyVehiclesCount,
				enemyLastReport = enemyLastReport,
				inLOS = inLOS,
				areaMiddle = area.middle,
				updateFrame = updateFrame,
			}			
		else
			newValues[a] = {
				allySoldiersCount = oldData.allySoldiersCount,
				allyVehiclesCount = oldData.allyVehiclesCount,
				enemySoldierCount = oldData.enemySoldierCount,
				enemyVehiclesCount = oldData.enemyVehiclesCount,
				enemyLastReport = oldData.enemyLastReport,
				inLOS = inLOS,
				areaMiddle = area.middle,
				updateFrame = oldData.updateFrame,
			}
		end
	end
	return newValues
end