local sensorInfo = {
	name = "FrontLineMap",
	desc = "Maintain private front line map for given group",
	author = "PepeAmpere",
	date = "2019-01-16",
	license = "MIT",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load
attach.Module(modules, "tableExt") --  table module

local EVAL_PERIOD_DEFAULT = -1 -- this sensor is not caching any values, it evaluates itself on every request

local mapDefs = Sensors.FrontLineMapDefs()
local TILE_SIZE = mapDefs.TILE_SIZE
local mapX = mapDefs.mapX
local mapZ = mapDefs.mapZ
local mapTilesX = mapDefs.mapTilesX
local mapTilesZ = mapDefs.mapTilesZ
local mapArraySize = mapDefs.mapArraySize
local mapCoordinates = mapDefs.mapCoordinates

local GetTileIndexForPosition = mapDefs.GetTileIndexForPosition
local GetCornerIndexes = mapDefs.GetCornerIndexes
local GetUpdatePool = mapDefs.GetUpdatePool
local NewTile = mapDefs.NewTile

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- @description Recalculate values for specified areas
-- @argument currentMap [array] previous full threat map
-- @argument currentPosition [Vec3|optional] center of update radius (in not provided, we take position of the point-unit)
-- @argument tileRadius [number] radius of map update in tiles
-- @return currentMap [array] updated version of map
-- @comment map is supposed to be stored
return function(currentMap, currentPosition, tileRadius)
	if (units == nil or #units < 1) then return currentMap end
	if (currentMap == nil) then
		currentMap = {}
		if (Script.LuaUI('frontLineMap_init')) then
			Script.LuaUI.frontLineMap_init(currentMap, mapCoordinates)
		end
	end
	
	-- if not provided, default value
	if (currentPosition == nil) then
		local x,y,z = Spring.GetUnitPosition(units[1])
		currentPosition = Vec3(x, y, z)
	end
	
	-- select tiles to be updated
	local updatePoolCoordinates, updatePoolOldData, updatePoolIndexes = GetUpdatePool(currentMap, currentPosition, tileRadius)
	
	local updates = Sensors.FrontLineMapUpdate(updatePoolCoordinates, updatePoolOldData)
	if (Script.LuaUI('frontLineMap_update')) then
		Script.LuaUI.frontLineMap_update(updates, updatePoolIndexes)
	end
	
	for i=1, #updatePoolIndexes do
		local mapIndex = updatePoolIndexes[i]
		-- init the tile only if needed
		if currentMap[mapIndex] == nil then
			currentMap[mapIndex] = NewTile()
		end
		if (updates[i] ~= nil) then
			for k,v in pairs(updates[i]) do
				currentMap[mapIndex][k] = v
			end
		end
	end
	
	return currentMap
end
